import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, TouchableOpacity, Text, TextInput} from 'react-native';
import {Brand, Button, Loader} from '@components';
import {useTheme} from '@theme';
import {addUser} from '@store/home/Users';
import {useTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CheckConnection from '@utils/CheckConnection';
import {isValidAmount} from '@utils/validate';
import {showMessage, hideMessage} from 'react-native-flash-message';
import RazorpayCheckout from 'react-native-razorpay';
import {Config} from '@config';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const IndexPaymentContainer = ({navigation}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const mounted = useRef();

  const addUserResponse = useSelector((state) => state.home.addUser.data);
  const addUserLoading = useSelector((state) => state.home.addUser.loading);
  const addUserError = useSelector((state) => state.home.addUser.error);
  const doRedirect = (screen) => {
    navigation.navigate(screen);
  };
  const isConnected = CheckConnection();

  const [amount, setName] = useState('');
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
    } else {
      // do componentDidUpdate logic
      if (addUserResponse) {
        showMessage({
          message: 'Add User Success',
          type: 'success',
        });
      }
      if (addUserError) {
        showMessage({
          message: addUserError.message,
          type: 'danger',
        });
      }
    }
  }, [addUserResponse, addUserError]);

  const doPay = () => {
    if (amount.trim() === '') {
      showMessage({
        message: 'Enter Amount',
        type: 'danger',
      });
      return;
    }

    if (!isValidAmount(amount)) {
      showMessage({
        message: 'Enter Valid Amount',
        type: 'danger',
      });
      return;
    }

    if (!isConnected) {
      showMessage({
        message: 'No Internet',
        type: 'danger',
      });
      return;
    }

    var options = {
      description: 'Credits towards consultation',
      image: 'https://i.imgur.com/3g7nmJC.png',
      currency: 'INR',
      key: Config.RAZORPAY_API_KEY, // Your api key
      amount: amount,
      name: 'foo',
      prefill: {
        email: 'void@razorpay.com',
        contact: '9191919191',
        name: 'Razorpay Software',
      },
      theme: {color: Colors.primary},
    };
    RazorpayCheckout.open(options)
      .then((data) => {
        // handle success
        alert(`Success: ${data.razorpay_payment_id}`);
      })
      .catch((error) => {
        // handle failure
        alert(`Error: ${error.code} | ${error.description}`);
      });
    // dispatch(addUser.action({amount: name, job: job}));
  };

  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <KeyboardAwareScrollView
        style={[Layout.fill]}
        keyboardShouldPersistTaps="always">
        <View style={[Layout.fill]}>
          <TextInput
            placeholder={t('amount')}
            onChangeText={(text) => setName(text)}
            keyboardType="default"
            value={amount}
            style={[Common.textInput, Fonts.textRegular]}
          />
        </View>
      </KeyboardAwareScrollView>
      <TouchableOpacity
        style={[Layout.selfCenter]}
        onPress={() => doRedirect('AddCard')}>
        <Text style={[Common.text, Fonts.textLarge, {color: Colors.primary}]}>
          {t('add_card')}
        </Text>
      </TouchableOpacity>
      {addUserLoading && <Loader />}
      <TouchableOpacity onPress={() => doPay()}>
        <Button title={t('button.pay')} />
      </TouchableOpacity>
    </View>
  );
};

export default IndexPaymentContainer;
