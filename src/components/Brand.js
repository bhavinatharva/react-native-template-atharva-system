import React from 'react';
import {View, Image} from 'react-native';
import {useTheme} from '@theme';

const Brand = ({height = 200, width = 200, mode = 'contain', source}) => {
  const {Layout, Images} = useTheme();

  return (
    <View style={{height, width}}>
      <Image style={Layout.fullSize} source={source} resizeMode={mode} />
    </View>
  );
};

export default Brand;
