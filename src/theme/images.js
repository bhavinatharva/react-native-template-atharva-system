/**
 *
 * @param Theme can be spread like {Colors, NavigationColors, Gutters, Layout, Common, ...args}
 * @return {*}
 */
export default function () {
  return {
    splash: require('@assets/images/splash_light.png'),
    logo: require('@assets/images/TOM.png'),
    user_placeholder: require('@assets/images/user_placeholder_light.png'),
    tab_users: require('@assets/images/tab_users.png'),
    home: require('@assets/images/home.png'),
    logout: require('@assets/images/logout.png'),
    menu: require('@assets/images/menu.png'),
    add_user: require('@assets/images/add_user.png'),
    edit_user: require('@assets/images/edit_user.png'),
    info: require('@assets/images/info.png'),
    facebook: require('@assets/images/facebook.png'),
    google: require('@assets/images/google.png'),
    favorite: require('@assets/images/favorite.png'),
    apple: require('@assets/images/apple.png'),
    change_password: require('@assets/images/change_password.png'),
    chat: require('@assets/images/chat.png'),
    send: require('@assets/images/send.png'),
    conversations: require('@assets/images/conversations.png'),
    map: require('@assets/images/map.png'),
    bike_marker: require('@assets/images/bike_marker.png'),
    payment: require('@assets/images/payment.png'),
    websocket: require('@assets/images/websocket.png'),
    dating: require('@assets/images/dating.png'),
    one: require('@assets/images/1.jpg'),
    two: require('@assets/images/2.jpg'),
    three: require('@assets/images/3.jpg'),
    four: require('@assets/images/4.jpg'),
    five: require('@assets/images/5.jpg'),
  };
}
