/**
 * This file contains the application's variables.
 *
 * Define color, sizes, etc. here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

/**
 * Colors
 */
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export const Colors = {
  // Example colors:
  transparent: 'rgba(0,0,0,0)',
  black:'#000000',
  dimBlack:'rgba(0,0,0,0.5)',
  inputBackground: '#FFFFFF',
  white: '#ffffff',
  text: '#272424',
  primary: '#C2D3DA',
  primaryDark: '#81A3A7',
  accent: '#585A56',
  success: '#28a745',
  error: '#dc3545',
  tabActive: '#272424',
  tabInactive: '#E7E4E4',
}

export const NavigationColors = {
  primary: Colors.text,
}

/**
 * FontSize
 */
export const FontSize = {
  small: RFPercentage(1.8),
  regular: RFPercentage(2),
  large: RFPercentage(3),
}

/**
 * Metrics Sizes
 */
const tiny = 5 // 10
const small = tiny * 2 // 10
const regular = tiny * 3 // 15
const large = regular * 2 // 30
export const MetricsSizes = {
  tiny,
  small,
  regular,
  large,
}
