import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, TouchableOpacity, Text, TextInput} from 'react-native';
import {Brand, Button, Loader} from '@components';
import {useTheme} from '@theme';
import {addUser} from '@store/home/Users';
import {useTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CheckConnection from '@utils/CheckConnection';
import {showMessage, hideMessage} from 'react-native-flash-message';

const IndexAddUserContainer = () => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const mounted = useRef();

  const addUserResponse = useSelector((state) => state.home.addUser.data);
  const addUserLoading = useSelector((state) => state.home.addUser.loading);
  const addUserError = useSelector((state) => state.home.addUser.error);

  const isConnected = CheckConnection();

  const [name, setName] = useState('');
  const [job, setJob] = useState('');
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
    } else {
      // do componentDidUpdate logic
      if (addUserResponse) {
        showMessage({
          message: 'Add User Success',
          type: 'success',
        });
      }
      if (addUserError) {
        showMessage({
          message: addUserError.message,
          type: 'danger',
        });
      }
    }
  }, [addUserResponse, addUserError]);

  const doAddUser = () => {
    if (name.trim() === '') {
      showMessage({
        message: 'Enter Name',
        type: 'danger',
      });
      return;
    }
    if (job.trim() === '') {
      showMessage({
        message: 'Enter Job',
        type: 'danger',
      });
      return;
    }
    if (!isConnected) {
      showMessage({
        message: 'No Internet',
        type: 'danger',
      });
      return;
    }
    dispatch(addUser.action({name: name, job: job}));
  };

  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <KeyboardAwareScrollView
        style={[Layout.fill]}
        keyboardShouldPersistTaps="always">
        <View style={[Layout.fill]}>
          <TextInput
            placeholder={t('name')}
            onChangeText={(text) => setName(text)}
            keyboardType="default"
            value={name}
            style={[Common.textInput, Fonts.textRegular]}
          />
          <TextInput
            placeholder={t('job')}
            onChangeText={(text) => setJob(text)}
            keyboardType="default"
            value={job}
            style={[Common.textInput, Fonts.textRegular]}
          />
        </View>
      </KeyboardAwareScrollView>
      {addUserLoading && <Loader />}
      <TouchableOpacity onPress={() => doAddUser()}>
        <Button title={t('button.add_user')} />
      </TouchableOpacity>
    </View>
  );
};

export default IndexAddUserContainer;
