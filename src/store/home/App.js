import {
  buildAsyncState,
  buildAsyncReducers,
  buildAsyncActions,
} from '@thecodingmachine/redux-toolkit-wrapper';
import {isLogin, isIntro, currentUser} from '@services/home/App';

const checkLogin = {
  initialState: buildAsyncState('checkLogin'),
  action: buildAsyncActions('checkLogin', isLogin),
  reducers: buildAsyncReducers({
    itemKey: 'checkLogin',
  }),
};
const checkIntro = {
  initialState: buildAsyncState('checkIntro'),
  action: buildAsyncActions('checkIntro', isIntro),
  reducers: buildAsyncReducers({
    itemKey: 'checkIntro',
  }),
};
const getCurrentUser = {
  initialState: buildAsyncState('getCurrentUser'),
  action: buildAsyncActions('getCurrentUser', currentUser),
  reducers: buildAsyncReducers({
    itemKey: 'getCurrentUser',
  }),
};

export {checkLogin, checkIntro, getCurrentUser};
