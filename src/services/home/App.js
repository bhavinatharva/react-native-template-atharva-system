import AsyncStorage from '@react-native-async-storage/async-storage';

const isLogin = async (body) => {
  let checkLogin = false;
  let value = await AsyncStorage.getItem('isLogin');
  if (value) {
    checkLogin = value == 'true';
  }
  return checkLogin;
};

const setLogin = async (isLogin) => {
  AsyncStorage.setItem('isLogin', isLogin.toString());
};

const isIntro = async (body) => {
  let checkIntro = false;
  let value = await AsyncStorage.getItem('isIntro');
  if (value) {
    checkIntro = value == 'true';
  }
  return checkIntro;
};
const setIntro = async (isIntro) => {
  AsyncStorage.setItem('isIntro', isIntro.toString());
};
const getCurrentUser = async (body) => {
  let currentUser = undefined;
  let value = await AsyncStorage.getItem('currentUser');
  if (value) {
    currentUser = JSON.parse(value);
  }
  return currentUser;
};
const setCurrentUser = async (currentUser) => {
  AsyncStorage.setItem('currentUser', JSON.stringify(currentUser));
};

export {isLogin, setLogin, isIntro, setIntro, getCurrentUser, setCurrentUser};
