import React from 'react';
import {View, Image} from 'react-native';
import {useTheme} from '@theme';

const TabIcon = ({
  height = 200,
  width = 200,
  mode = 'contain',
  source,
  color,
}) => {
  const {Layout, Images} = useTheme();

  return (
    <View style={{height, width}}>
      <Image
        style={[Layout.fullSize, {tintColor: color}]}
        source={source}
        resizeMode={mode}
      />
    </View>
  );
};

export default TabIcon;
