import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, Text, FlatList, TouchableOpacity} from 'react-native';
import {HomeListItem, Loader, ListButton} from '@components';
import {useTheme} from '@theme';
import {users as GetUsers} from '@store/home/Users';
import {useTranslation} from 'react-i18next';
import {showMessage, hideMessage} from 'react-native-flash-message';
import CheckConnection from '../../utils/CheckConnection';

const IndexHomeContainer = ({navigation}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const isConnected = CheckConnection();
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [users, setUsers] = useState([]);
  const [page, setPage] = useState(1);
  const [per_page, setPerPage] = useState(10);
  const [lastPage, setLastPage] = useState(1);

  const usersResponse = useSelector((state) => state.home.users.data);
  const usersLoading = useSelector((state) => state.home.users.loading);
  const usersError = useSelector((state) => state.home.users.error);
  const mounted = useRef();
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
      dispatch(GetUsers.action({page: page, per_page: per_page}));
    } else {
      // do componentDidUpdate logic
      if (usersError) {
        showMessage({
          message: usersError.message,
          type: 'danger',
        });
      }
      if (usersResponse) {
        if (page === 1) {
          setUsers(usersResponse.data);
        } else {
          setUsers((old) => [...old, ...usersResponse.data]);
        }

        setLastPage(usersResponse.total_pages);
      }

      setRefreshing(false);
      setLoading(false);
    }
  }, [usersResponse, usersError]);

  const handleRefresh = () => {
    setPage(1);
    setRefreshing(true);
    dispatch(GetUsers.action({page: page, per_page: per_page}));
  };
  const handleLoadMore = () => {
    if (page !== lastPage) {
      setPage(page + 1);
      dispatch(GetUsers.action({page: page + 1, per_page: per_page}));
    }
  };
  const doEditDetail = (item) => {
    navigation.navigate('EditUser', {item: item, isEdit: true});
  };
  const doOpenDetail = (item) => {
    navigation.navigate('UserDetail', {item: item});
  };
  return (
    <View style={[Layout.fill]}>
      <Text style={[Common.text, Fonts.textLarge]}>{t('users')}</Text>
      <FlatList
        numColumns={2}
        data={users}
        onRefresh={handleRefresh}
        onEndReached={handleLoadMore}
        onEndReachedThreshold={0}
        refreshing={refreshing}
        renderItem={({item}) => (
          <HomeListItem
            email={item.email}
            first_name={item.first_name}
            last_name={item.last_name}
            avatar={item.avatar}
            doOpenDetail={() => doOpenDetail(item)}
          />
        )}
        keyExtractor={(item, index) => index.toString()}
        columnWrapperStyle={{justifyContent: 'space-evenly', flex: 1}}
      />

      {usersLoading && <Loader />}
    </View>
  );
};

export default IndexHomeContainer;
