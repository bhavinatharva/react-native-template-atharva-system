import api, {handleError} from '@services';

const doGetUsers = async (body) => {
  let response = {data: undefined};
  try {
    let urlParameters = Object.entries(body)
      .map((e) => e.join('='))
      .join('&');
    let result = await api.get(`users?${urlParameters}`);
    response.data = result.data;
  } catch (error) {
    return handleError({message: error.data.error});
  }
  return response;
};

const doGetUserDetail = async (body) => {
  let response = {data: undefined};
  try {
    let result = await api.get(`users/${body.id}`);
    response.data = result.data;
  } catch (error) {
    return handleError({message: error.data.error});
  }
  return response;
};

const doAddUser = async (body) => {
  let response = {data: undefined};
  try {
    let result = await api.post('users', body);
    response.data = result.data;
  } catch (error) {
    return handleError({message: error.data.error});
  }
  return response;
};

const doEditUser = async (body) => {
  let response = {data: undefined};
  try {
    let result = await api.put(`users/${body.id}`, body);
    response.data = result.data;
  } catch (error) {
    return handleError({message: error.data.error});
  }
  return response;
};

export {doGetUsers, doGetUserDetail, doAddUser, doEditUser};
