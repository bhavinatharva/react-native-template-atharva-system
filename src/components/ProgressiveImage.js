import React from 'react';
import {View, StyleSheet, Animated} from 'react-native';
import {useTheme} from '@theme';
const ProgressiveImage = ({thumbnailSource, source, style,thumbStyle}) => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const thumbnailAnimated = new Animated.Value(0);

  const imageAnimated = new Animated.Value(0);
  const handleThumbnailLoad = () => {
    Animated.timing(thumbnailAnimated, {
      toValue: 1,
      useNativeDriver: true,
    }).start();
  };
  const onImageLoad = () => {
    Animated.timing(imageAnimated, {
      toValue: 1,
      useNativeDriver: true,
    }).start();
  };
  return (
    <View
      style={{
        backgroundColor: '#e1e4e8',
      }}>
      <Animated.Image
        source={thumbnailSource}
        style={[style, {opacity: thumbnailAnimated},thumbStyle]}
        onLoad={() => handleThumbnailLoad()}
        blurRadius={1}
      />
      <Animated.Image
        source={source}
        style={[
          {
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            top: 0,
            opacity: imageAnimated,
          },
          style,
        ]}
        onLoad={() => onImageLoad()}
      />
    </View>
  );
};

export default ProgressiveImage;
