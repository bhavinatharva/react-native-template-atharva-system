import { buildSlice } from '@thecodingmachine/redux-toolkit-wrapper'
import InitStartup from './init'

export default buildSlice('startup', [InitStartup]).reducer
