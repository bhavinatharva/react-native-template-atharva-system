import React, {PureComponent} from 'react';
import {Text, View} from 'react-native';

class ErrorBoundary extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {error: null, errorInfo: null};
  }
  componentDidCatch(error, errorInfo) {
    // Catch errors in any components below and re-render with error message
    this.setState({
      error: error,
      errorInfo: errorInfo,
    });
    // You can also log error messages to an error reporting service here
  }
  render() {
    if (this.state.errorInfo) {
      return (
        <View style={{flex: 1}}>
          <Text> Something went wrong. </Text>
          <Text> {JSON.stringify(this.state.error)} </Text>
          <Text> {JSON.stringify(this.state.errorInfo)} </Text>
        </View>
      );
    } else {
      return this.props.children;
    }
  }
}

export default ErrorBoundary;
