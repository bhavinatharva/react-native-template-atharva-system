import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  Animated,
  PanResponder,
  TouchableOpacity,
} from 'react-native';
import {Brand, Button, Loader} from '@components';
import {useTheme} from '@theme';
import {addUser} from '@store/home/Users';
import {useTranslation} from 'react-i18next';
import CheckConnection from '@utils/CheckConnection';
import {showMessage, hideMessage} from 'react-native-flash-message';

const IndexDatingContainer = () => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const mounted = useRef();
  const [users, setUsers] = useState([
    {id: '1', uri: Images.one},
    {id: '2', uri: Images.two},
    {id: '3', uri: Images.three},
    {id: '4', uri: Images.four},
    {id: '5', uri: Images.five},
  ]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const SCREEN_HEIGHT = Dimensions.get('window').height;
  const SCREEN_WIDTH = Dimensions.get('window').width;
  const isConnected = CheckConnection();
  let position = new Animated.ValueXY({x: 0, y: 10});
  let rotate = position.x.interpolate({
    inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
    outputRange: ['-30deg', '0deg', '10deg'],
    extrapolate: 'clamp',
  });
  let rotateAndTranslate = {
    transform: [
      {
        rotate: rotate,
      },
      ...position.getTranslateTransform(),
    ],
  };
  let likeOpacity = position.x.interpolate({
    inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
    outputRange: [0, 0, 1],
    extrapolate: 'clamp',
  });
  let dislikeOpacity = position.x.interpolate({
    inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
    outputRange: [1, 0, 0],
    extrapolate: 'clamp',
  });
  let nextCardOpacity = position.x.interpolate({
    inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
    outputRange: [1, 0, 1],
    extrapolate: 'clamp',
  });
  let nextCardScale = position.x.interpolate({
    inputRange: [-SCREEN_WIDTH / 2, 0, SCREEN_WIDTH / 2],
    outputRange: [1, 0.8, 1],
    extrapolate: 'clamp',
  });
  let panResponder = PanResponder.create({
    onStartShouldSetPanResponder: (evt, gestureState) => true,
    onPanResponderMove: (evt, gestureState) => {
      position.setValue({x: gestureState.dx, y: gestureState.dy});
    },
    onPanResponderRelease: (evt, gestureState) => {
      if (gestureState.dx > 120) {
        Animated.spring(position, {
          toValue: {x: SCREEN_WIDTH + 100, y: gestureState.dy},
          useNativeDriver: true,
        }).start(() => {
          setCurrentIndex(currentIndex + 1);
          position.setValue({x: 0, y: 0});
        });
      } else if (gestureState.dx < -120) {
        Animated.spring(position, {
          toValue: {x: -SCREEN_WIDTH - 100, y: gestureState.dy},
          useNativeDriver: true,
        }).start(() => {
          setCurrentIndex(currentIndex + 1);
          position.setValue({x: 0, y: 0});
        });
      } else {
        Animated.spring(position, {
          toValue: {x: 0, y: 0},
          friction: 4,
          useNativeDriver: true,
        }).start();
      }
    },
  });
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
    } else {
      // do componentDidUpdate logic
    }
  }, []);

  const renderUsers = () => {
    return users
      .map((item, i) => {
        if (i < currentIndex) {
          return null;
        } else if (i == currentIndex) {
          return (
            <Animated.View
              {...panResponder.panHandlers}
              key={item.id}
              style={[
                rotateAndTranslate,
                {
                  height: SCREEN_HEIGHT - 120,
                  width: SCREEN_WIDTH - 20,
                  padding: 10,
                  position: 'absolute',
                  alignSelf: 'center',
                },
              ]}>
              <Animated.View
                style={{
                  opacity: likeOpacity,
                  transform: [{rotate: '-30deg'}],
                  position: 'absolute',
                  top: 50,
                  left: 40,
                  zIndex: 1000,
                }}>
                <Text
                  style={{
                    borderWidth: 1,
                    borderColor: 'green',
                    color: 'green',
                    fontSize: 32,
                    fontWeight: '800',
                    padding: 10,
                  }}>
                  LIKE
                </Text>
              </Animated.View>

              <Animated.View
                style={{
                  opacity: dislikeOpacity,
                  transform: [{rotate: '30deg'}],
                  position: 'absolute',
                  top: 50,
                  right: 40,
                  zIndex: 1000,
                }}>
                <Text
                  style={{
                    borderWidth: 1,
                    borderColor: 'red',
                    color: 'red',
                    fontSize: 32,
                    fontWeight: '800',
                    padding: 10,
                  }}>
                  NOPE
                </Text>
              </Animated.View>

              <Image
                style={{
                  flex: 1,
                  height: null,
                  width: null,
                  resizeMode: 'cover',
                  borderRadius: 20,
                }}
                source={item.uri}
              />
            </Animated.View>
          );
        } else {
          return (
            <Animated.View
              key={item.id}
              style={[
                {
                  opacity: nextCardOpacity,
                  transform: [{scale: nextCardScale}],
                  height: SCREEN_HEIGHT - 120,
                  width: SCREEN_WIDTH,
                  padding: 10,
                  position: 'absolute',
                },
              ]}>
              <Animated.View
                style={{
                  opacity: 0,
                  transform: [{rotate: '-30deg'}],
                  position: 'absolute',
                  top: 50,
                  left: 40,
                  zIndex: 1000,
                }}>
                <Text
                  style={{
                    borderWidth: 1,
                    borderColor: 'green',
                    color: 'green',
                    fontSize: 32,
                    fontWeight: '800',
                    padding: 10,
                  }}>
                  LIKE
                </Text>
              </Animated.View>

              <Animated.View
                style={{
                  opacity: 0,
                  transform: [{rotate: '30deg'}],
                  position: 'absolute',
                  top: 50,
                  right: 40,
                  zIndex: 1000,
                }}>
                <Text
                  style={{
                    borderWidth: 1,
                    borderColor: 'red',
                    color: 'red',
                    fontSize: 32,
                    fontWeight: '800',
                    padding: 10,
                  }}>
                  NOPE
                </Text>
              </Animated.View>

              <Image
                style={{
                  flex: 1,
                  height: null,
                  width: null,
                  resizeMode: 'cover',
                  borderRadius: 20,
                }}
                source={item.uri}
              />
            </Animated.View>
          );
        }
      })
      .reverse();
  };

  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>{renderUsers()}</View>
  );
};

export default IndexDatingContainer;
