import React, {useEffect} from 'react';
import {ActivityIndicator, View, Text} from 'react-native';
import {useTheme} from '@theme';
import {useDispatch} from 'react-redux';
import InitStartup from '@store/startup/init';
import {useTranslation} from 'react-i18next';
import {Brand} from '@components';

const IndexSplashContainer = () => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();

  const {t} = useTranslation();

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(InitStartup.action());
  }, [dispatch]);

  return (
    <View style={[Layout.fill, Layout.colCenter, Common.white]}>
      <Brand source={Images.splash} />
      <Text style={[Fonts.textCenter, Fonts.textLarge]}>{t('app_name')}</Text>
    </View>
  );
};

export default IndexSplashContainer;
