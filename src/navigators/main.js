import React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import {TabNavigator} from './index';

import {TabIcon} from '@components';
import {useTheme} from '@theme';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {setLogin} from '@services/home/App';
import {navigate, navigateAndReset} from '@navigators/root';

const Drawer = createDrawerNavigator();

// @refresh reset
const doAddUser = () => {
  navigate('AddUser', {});
};
const doRedirect = (screen) => {
  navigate(screen, {});
};
const doLogout = () => {
  setLogin(false);
  navigateAndReset('Login');
};
const MainNavigator = () => {
  const {Layout, Images, Colors} = useTheme();
  return (
    <Drawer.Navigator
      drawerContent={(props) => {
        return (
          <DrawerContentScrollView {...props}>
            <DrawerItemList {...props} />
            <DrawerItem
              label="Add User"
              onPress={() => doAddUser()}
              icon={(focused, color, size) => (
                <TabIcon
                  source={Images.add_user}
                  height={wp(6)}
                  width={wp(6)}
                  color={focused ? Colors.tabActive : Colors.tabInactive}
                />
              )}
            />
            <DrawerItem
              label="Payment"
              onPress={() => doRedirect('Payment')}
              icon={(focused, color, size) => (
                <TabIcon
                  source={Images.payment}
                  height={wp(6)}
                  width={wp(6)}
                  color={focused ? Colors.tabActive : Colors.tabInactive}
                />
              )}
            />
            <DrawerItem
              label="WebSocket"
              onPress={() => doRedirect('WebSocket')}
              icon={(focused, color, size) => (
                <TabIcon
                  source={Images.websocket}
                  height={wp(6)}
                  width={wp(6)}
                  color={focused ? Colors.tabActive : Colors.tabInactive}
                />
              )}
            />
            <DrawerItem
              label="Change Password"
              onPress={() => doRedirect('ChangePassword')}
              icon={(focused, color, size) => (
                <TabIcon
                  source={Images.change_password}
                  height={wp(6)}
                  width={wp(6)}
                  color={focused ? Colors.tabActive : Colors.tabInactive}
                />
              )}
            />
            <DrawerItem
              label="Dating"
              onPress={() => doRedirect('Dating')}
              icon={(focused, color, size) => (
                <TabIcon
                  source={Images.dating}
                  height={wp(6)}
                  width={wp(6)}
                  color={focused ? Colors.tabActive : Colors.tabInactive}
                />
              )}
            />
            <DrawerItem
              label="Logout"
              onPress={() => doLogout()}
              icon={(focused, color, size) => (
                <TabIcon
                  source={Images.logout}
                  height={wp(6)}
                  width={wp(6)}
                  color={focused ? Colors.tabActive : Colors.tabInactive}
                />
              )}
            />
          </DrawerContentScrollView>
        );
      }}>
      <Drawer.Screen
        name="Home"
        component={TabNavigator}
        options={{
          title: 'Home',
          drawerIcon: ({focused, color, size}) => (
            <TabIcon
              source={Images.home}
              height={wp(6)}
              width={wp(6)}
              color={focused ? Colors.tabActive : Colors.tabInactive}
            />
          ),
        }}
      />
    </Drawer.Navigator>
  );
};

export default MainNavigator;
