import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  IndexUsersContainer,
  IndexHomeContainer,
  IndexConversationsContainer,
  IndexMapContainer
} from '@containers';
import {TabIcon} from '@components';
import {useTheme} from '@theme';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const Tab = createBottomTabNavigator();

// @refresh reset
const TabNavigator = () => {
  const {Layout, Images, Colors} = useTheme();
  return (
    <Tab.Navigator initialRouteName="Users">
      <Tab.Screen
        name="Users"
        component={IndexUsersContainer}
        options={{
          tabBarLabel: 'Users',
          tabBarIcon: ({focused, color, size}) => (
            <TabIcon
              source={Images.tab_users}
              height={wp(6)}
              width={wp(6)}
              color={focused ? Colors.tabActive : Colors.tabInactive}
            />
          ),
        }}
      />
      <Tab.Screen
        name="UsersGrid"
        component={IndexHomeContainer}
        options={{
          tabBarLabel: 'Users Grid',
          tabBarIcon: ({focused, color, size}) => (
            <TabIcon
              source={Images.tab_users}
              height={wp(6)}
              width={wp(6)}
              color={focused ? Colors.tabActive : Colors.tabInactive}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Conversations"
        component={IndexConversationsContainer}
        options={{
          tabBarLabel: 'Conversations',
          tabBarIcon: ({focused, color, size}) => (
            <TabIcon
              source={Images.conversations}
              height={wp(6)}
              width={wp(6)}
              color={focused ? Colors.tabActive : Colors.tabInactive}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Map"
        component={IndexMapContainer}
        options={{
          tabBarLabel: 'Map',
          tabBarIcon: ({focused, color, size}) => (
            <TabIcon
              source={Images.map}
              height={wp(6)}
              width={wp(6)}
              color={focused ? Colors.tabActive : Colors.tabInactive}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default TabNavigator;
