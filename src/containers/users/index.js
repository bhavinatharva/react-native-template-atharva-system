import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, Text, FlatList, TouchableOpacity} from 'react-native';
import {SwipeListView} from 'react-native-swipe-list-view';
import {UserListItem, Loader, ListButton} from '@components';
import {useTheme} from '@theme';
import {users as GetUsers} from '@store/home/Users';
import {useTranslation} from 'react-i18next';
import {showMessage, hideMessage} from 'react-native-flash-message';
import CheckConnection from '../../utils/CheckConnection';

const IndexUsersContainer = ({navigation}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const isConnected = CheckConnection();
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [users, setUsers] = useState([]);
  const [page, setPage] = useState(1);
  const [per_page, setPerPage] = useState(10);
  const [lastPage, setLastPage] = useState(1);

  const usersResponse = useSelector((state) => state.home.users.data);
  const usersLoading = useSelector((state) => state.home.users.loading);
  const usersError = useSelector((state) => state.home.users.error);
  const mounted = useRef();
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
      dispatch(GetUsers.action({page: page, per_page: per_page}));
    } else {
      // do componentDidUpdate logic
      if (usersError) {
        showMessage({
          message: usersError.message,
          type: 'danger',
        });
      }
      if (usersResponse) {
        if (page === 1) {
          setUsers(usersResponse.data);
        } else {
          setUsers((old) => [...old, ...usersResponse.data]);
        }
        setLastPage(usersResponse.total_pages);
      }

      setRefreshing(false);
      setLoading(false);
    }
  }, [usersResponse, usersError]);

  const handleRefresh = () => {
    setPage(1);
    setRefreshing(true);
    dispatch(GetUsers.action({page: page, per_page: per_page}));
  };
  const handleLoadMore = () => {
    if (page !== lastPage) {
      setPage(page + 1);
      dispatch(GetUsers.action({page: page + 1, per_page: per_page}));
    }
  };
  const doEditDetail = (item) => {
    navigation.navigate('EditUser', {item: item, isEdit: true});
  };
  const doOpenDetail = (item) => {
    navigation.navigate('UserDetail', {item: item});
  };
  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <Text style={[Common.text, Fonts.textLarge]}>{t('users')}</Text>
      <View style={[Layout.fill]}>
        <SwipeListView
          data={users}
          onRefresh={handleRefresh}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.01}
          refreshing={refreshing}
          renderItem={({item}) => (
            // <TouchableOpacity >
            <UserListItem
              email={item.email}
              first_name={item.first_name}
              last_name={item.last_name}
              avatar={item.avatar}
            />
            // </TouchableOpacity>
          )}
          renderHiddenItem={(data, rowMap) => (
            <View style={[Common.hiddenItem]}>
              <TouchableOpacity
                style={{marginEnd: 10}}
                onPress={() => doEditDetail(data.item)}>
                <ListButton source={Images.edit_user} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => doOpenDetail(data.item)}>
                <ListButton source={Images.info} />
              </TouchableOpacity>
            </View>
          )}
          leftOpenValue={75}
          rightOpenValue={-150}
          disableRightSwipe={true}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>

      {usersLoading && <Loader />}
    </View>
  );
};

export default IndexUsersContainer;
