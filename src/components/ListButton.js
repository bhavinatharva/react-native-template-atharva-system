import React from 'react';
import {View, Text, Image} from 'react-native';
import {useTheme} from '@theme';

const ListButton = ({source, mode = 'contain'}) => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  return (
    <View style={[Common.ListButton]}>
      <Image source={source} resizeMode={mode} style={Common.listImage} />
    </View>
  );
};

export default ListButton;
