const Colors = {
  text: '#D8DBDB',
  inputBackground: 'gray',
  primary: '#3D2C25',
  primaryDark: '#7E5C58',
  accent: '#A7A5A9',
}

const NavigationColors = {
  primary: Colors.primary,
}

export default {
  Colors,
  NavigationColors,
}
