import React from 'react';
import {Text, Image, View} from 'react-native';

import {useTheme} from '@theme';
const senderMessage = (text, isSender) => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  return (
    <View style={{alignItems: 'flex-end'}}>
      <View style={{...Common.chatSender}}>
        <Text
          style={{
            ...Fonts.textRegular,
            ...Common.fbTextStyle,
            ...{marginLeft: 0, marginRight: 0},
          }}>
          {text}
        </Text>
      </View>
    </View>
  );
};
const receiverMessage = (text, isSender) => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  return (
    <View style={{alignItems: 'flex-start'}}>
      <View style={{...Common.chatReceiver}}>
        <Text
          style={{
            ...Fonts.textRegular,
            ...Common.fbTextStyle,
            ...{marginLeft: 0, marginRight: 0},
          }}>
          {text}
        </Text>
      </View>
    </View>
  );
};
const Message = ({text, isSender}) => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  return (
    <View>
      {isSender
        ? senderMessage(text, isSender)
        : receiverMessage(text, isSender)}
    </View>
  );
};

export default Message;
