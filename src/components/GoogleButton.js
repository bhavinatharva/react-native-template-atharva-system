import React from 'react';
import {Text, Image, View} from 'react-native';

import {useTheme} from '@theme';

const GoogleButton = ({
  title = 'Sign in with Google',
  buttonViewStyle,
  logoStyle,
  textStyle,
}) => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  return (
    <View style={{...Common.googleStyle, ...buttonViewStyle}}>
      <Image
        source={Images.google}
        style={{...Common.fbIconStyle, ...logoStyle}}
      />
      <Text style={{...Fonts.textLarge,...Common.googleTextStyle, ...textStyle}}>
        {title}
      </Text>
    </View>
  );
};

export default GoogleButton;
