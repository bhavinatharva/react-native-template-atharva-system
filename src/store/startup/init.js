import {
  buildAsyncState,
  buildAsyncActions,
  buildAsyncReducers,
} from '@thecodingmachine/redux-toolkit-wrapper';
import {navigateAndSimpleReset, navigateAndReset} from '@navigators/root';
import DefaultTheme from '@store/theme/DefaultTheme';
import {isLogin, isIntro} from '@services/home/App';

export default {
  initialState: buildAsyncState(),
  action: buildAsyncActions('startup/init', async (args, {dispatch}) => {
    // Timeout to fake waiting some process
    // Remove it, or keep it if you want display a beautiful splash screen ;)
    await new Promise((resolve) => setTimeout(resolve, 1000));
    // Here we load the user 1 for example, but you can for example load the connected user
    await dispatch(DefaultTheme.action({theme: 'default', darkMode: null}));

    // Navigate and reset to the main navigator
    let checkLogin = await isLogin();
    if (checkLogin) {
      navigateAndReset('Home');
    } else {
      let checkIntro = await isIntro();
      if (checkIntro) {
        navigateAndSimpleReset('Login');
      } else {
        navigateAndSimpleReset('StartUp');
      }
    }
  }),
  reducers: buildAsyncReducers({itemKey: null}), // We do not want to modify some item by default
};
