import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, TouchableOpacity, TextInput} from 'react-native';
import {Brand, Button, Loader} from '@components';
import {useTheme} from '@theme';
import {forgotpassword} from '@store/auth/Authentication';
import {useTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CheckConnection from '@utils/CheckConnection';

import {showMessage, hideMessage} from 'react-native-flash-message';

const IndexForgotPasswordContainer = ({navigation}) => {
  const {t} = useTranslation();
  const mounted = useRef();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();

  const forgotPasswordResponse = useSelector(
    (state) => state.auth.forgotpassword.data,
  );
  const forgotPasswordLoading = useSelector(
    (state) => state.auth.forgotpassword.loading,
  );
  const forgotPasswordError = useSelector(
    (state) => state.auth.forgotpassword.error,
  );

  const isConnected = CheckConnection();

  const [email, setEmail] = useState('');
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
    } else {
      // do componentDidUpdate logic

      if (forgotPasswordError) {
        showMessage({
          message: forgotPasswordError.message,
          type: 'danger',
        });
      }
    }
  }, [forgotPasswordResponse, forgotPasswordError]);

  const doForgot = () => {
    if (email.trim() === '') {
      showMessage({
        message: 'Enter Email',
        type: 'danger',
      });
      return;
    }

    if (!isConnected) {
      showMessage({
        message: 'No Internet',
        type: 'danger',
      });
      return;
    }
    dispatch(forgotpassword.action({email: email}));
  };

  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <KeyboardAwareScrollView
        style={[Layout.fill]}
        keyboardShouldPersistTaps="always">
        <View style={[Layout.fill]}>
          <View style={[Layout.colCenter]}>
            <Brand source={Images.logo} />
          </View>
          <TextInput
            placeholder={t('email')}
            onChangeText={(text) => setEmail(text)}
            keyboardType="email-address"
            value={email}
            style={[Common.textInput, Fonts.textRegular]}
          />
        </View>
      </KeyboardAwareScrollView>
      {forgotPasswordLoading && <Loader />}
      <TouchableOpacity onPress={() => doForgot()}>
        <Button title={t('button.submit')} />
      </TouchableOpacity>
    </View>
  );
};

export default IndexForgotPasswordContainer;
