export const isValidAmount = (amount) => {
  let reg = /[1-9]\d*(?:\.\d{0,2})?/;
  return reg.test(parseFloat(amount));
};
