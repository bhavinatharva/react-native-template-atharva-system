import api, {handleError} from '@services';

const doLogin = async (body) => {
  let response = {data: undefined};
  try {
    let result = await api.post('login', body);
    response.data = result.data;
  } catch (error) {
    return handleError({message: error.data.error});
  }
  return response;
};

const doRegister = async (body) => {
  let response = {data: undefined};
  try {
    let result = await api.post('register', body);
    response.data = result.data;
  } catch (error) {
    return handleError({message: error.data.error});
  }
  return response;
};
const doForgot = async (body) => {
  let response = {data: undefined};
  try {
    let result = await api.post('login', body);
    response.data = result.data;
  } catch (error) {
    return handleError({message: error.data.error});
  }
  return response;
};
export {doLogin, doRegister,doForgot};
