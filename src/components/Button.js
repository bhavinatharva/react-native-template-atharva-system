import React from 'react';
import {View, Text} from 'react-native';
import {useTheme} from '@theme';

const Button = ({title = 'Title'}) => {
  const {Common, Fonts, Gutters, Layout} = useTheme();
  return (
    <View style={[Common.button, Layout.colCenter]}>
      <Text style={[Fonts.textLarge]}>{title}</Text>
    </View>
  );
};

export default Button;
