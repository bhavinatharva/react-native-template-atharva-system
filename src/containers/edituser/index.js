import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, TouchableOpacity, Text, TextInput} from 'react-native';
import {Brand, Button, Loader} from '@components';
import {useTheme} from '@theme';
import {editUser} from '@store/home/Users';
import {useTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CheckConnection from '@utils/CheckConnection';
import {showMessage, hideMessage} from 'react-native-flash-message';

const IndexEditUserContainer = ({route, navigation}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const mounted = useRef();
  const {item} = route.params;
  const editUserResponse = useSelector((state) => state.home.editUser.data);
  const editUserLoading = useSelector((state) => state.home.editUser.loading);
  const editUserError = useSelector((state) => state.home.editUser.error);

  const isConnected = CheckConnection();

  const [name, setName] = useState(`${item.first_name} ${item.last_name}`);
  const [job, setJob] = useState('');
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
    } else {
      // do componentDidUpdate logic
      if (editUserResponse) {
        showMessage({
          message: 'Edit User Success',
          type: 'success',
        });
        navigation.goBack(null);
      }
      if (editUserError) {
        showMessage({
          message: editUserError.message,
          type: 'danger',
        });
      }
    }
  }, [editUserResponse, editUserError]);

  const doEditUser = () => {
    if (name.trim() === '') {
      showMessage({
        message: 'Enter Name',
        type: 'danger',
      });
      return;
    }
    if (job.trim() === '') {
      showMessage({
        message: 'Enter Job',
        type: 'danger',
      });
      return;
    }
    if (!isConnected) {
      showMessage({
        message: 'No Internet',
        type: 'danger',
      });
      return;
    }
    dispatch(editUser.action({id: item.id, name: name, job: job}));
  };

  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <KeyboardAwareScrollView
        style={[Layout.fill]}
        keyboardShouldPersistTaps="always">
        <View style={[Layout.fill]}>
          <TextInput
            placeholder={t('name')}
            onChangeText={(text) => setName(text)}
            keyboardType="default"
            value={name}
            style={[Common.textInput, Fonts.textRegular]}
          />
          <TextInput
            placeholder={t('job')}
            onChangeText={(text) => setJob(text)}
            keyboardType="default"
            value={job}
            style={[Common.textInput, Fonts.textRegular]}
          />
        </View>
      </KeyboardAwareScrollView>
      {editUserLoading && <Loader />}
      <TouchableOpacity onPress={() => doEditUser()}>
        <Button title={t('button.submit')} />
      </TouchableOpacity>
    </View>
  );
};

export default IndexEditUserContainer;
