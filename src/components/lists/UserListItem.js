import React from 'react';
import {View, Image, Text} from 'react-native';
import {useTheme} from '@theme';

const UserListItem = ({email, first_name, last_name, avatar}) => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();

  return (
    <View style={[Common.listItem]}>
      <View style={Layout.rowHCenter}>
        <Image
          style={[Common.avatar]}
          source={avatar === '' ? Images.user_placeholder : {uri: avatar}}
        />
        <View style={Layout.fill}>
          <Text style={[Fonts.textRegular, Common.listItemText]}>
            Email : {email}
          </Text>
          <Text style={[Fonts.textRegular, Common.listItemText]}>
            Name : {first_name} {last_name}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default UserListItem;
