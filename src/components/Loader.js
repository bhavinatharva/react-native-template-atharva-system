import React from 'react';
import {View} from 'react-native';
import {useTheme} from '@theme';
import {BarIndicator } from 'react-native-indicators';
const Loader = ({}) => {
  const {Layout, Common, Colors} = useTheme();

  return (
    <View style={[Layout.fill, Layout.colCenter, Common.loader]}>
      <BarIndicator  color={Colors.white} count={5} />
    </View>
  );
};

export default Loader;
