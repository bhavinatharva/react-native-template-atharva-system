import {
  buildAsyncState,
  buildAsyncReducers,
  buildAsyncActions,
} from '@thecodingmachine/redux-toolkit-wrapper';
import {doLogin, doRegister, doForgot} from '@services/auth/Authentication';

const login = {
  initialState: buildAsyncState('login'),
  action: buildAsyncActions('doLogin', doLogin),
  reducers: buildAsyncReducers({
    itemKey: 'login',
    errorKey: 'login.error', // Optionally, if you scoped variables, you can use a key with dot notation
    loadingKey: 'login.loading',
  }),
};

const register = {
  initialState: buildAsyncState('register'),
  action: buildAsyncActions('doRegister', doRegister),
  reducers: buildAsyncReducers({
    itemKey: 'register',
    errorKey: 'register.error', // Optionally, if you scoped variables, you can use a key with dot notation
    loadingKey: 'register.loading',
  }),
};

const forgotpassword = {
  initialState: buildAsyncState('forgotpassword'),
  action: buildAsyncActions('doForgot', doForgot),
  reducers: buildAsyncReducers({
    itemKey: 'forgotpassword',
    errorKey: 'forgotpassword.error', // Optionally, if you scoped variables, you can use a key with dot notation
    loadingKey: 'forgotpassword.loading',
  }),
};

export {login, register, forgotpassword};
