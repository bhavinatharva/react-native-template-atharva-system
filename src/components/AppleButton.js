import React from 'react';
import {Text, Image, View} from 'react-native';

import {useTheme} from '@theme';

const AppleButton = ({
  title = 'Sign in with Apple',
  buttonViewStyle,
  logoStyle,
  textStyle,
}) => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  return (
    <View style={{...Common.appleStyle, ...buttonViewStyle}}>
      <Image
        source={Images.apple}
        style={{...Common.appleIconStyle, ...logoStyle}}
      />
      <Text style={{...Fonts.textLarge,...Common.fbTextStyle, ...textStyle}}>
        {title}
      </Text>
    </View>
  );
};

export default AppleButton;
