import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, FlatList} from 'react-native';
import {Loader, Message, MessageBar} from '@components';
import {useTheme} from '@theme';
import {getCurrentUser} from '@services/home/App';
import {useTranslation} from 'react-i18next';
import {showMessage, hideMessage} from 'react-native-flash-message';
import CheckConnection from '../../utils/CheckConnection';
import database from '@react-native-firebase/database';
import {Config} from '@config';

const IndexChatContainer = ({route, navigation}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const isConnected = CheckConnection();
  const {item, from} = route.params;

  const [email, setEmail] = useState('');
  const [conversationId, setConversationId] = useState('');
  const [lastMessage, setLastMessage] = useState('');
  const [lastMessageDate, setLastMessageDate] = useState('');
  const convRef = database().ref().child(`${Config.FIREBASE_CONVERSATIONS}`);

  const [messages, setMessages] = useState([]);

  const mounted = useRef();
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }

      if (from === 'UserDetail') {
        navigation.setOptions({
          title: `${item.first_name} ${item.last_name}`,
        });
        getCurrentUser().then((value) => {
          console.log(`value`, value);
          let conversations = {
            uniqueId: '',
            id: `${value.email}_${item.email}`,
            lastMessage: lastMessage,
            lastMessageSent: lastMessageDate,
            members: [value.email, item.email],
            type: 'private',
          };
          setEmail(value.email);
          convRef.orderByChild('members').once('value', (snapshot) => {
            console.log(`snapshot`, snapshot);
            if (snapshot.exists()) {
              snapshot.forEach((data) => {
                console.log(`data`, data.key, data.val().type);
                if (data.val().type === 'private') {
                  var includesAll = (array_to_check) =>
                    data
                      .val()
                      .members.reduce(
                        (accumulator, current) =>
                          accumulator && array_to_check.includes(current),
                        true,
                      );

                  if (includesAll(conversations.members)) {
                    console.log('yes');
                    let key = data.key;
                    setConversationId(key);
                    userChatList(key);
                    return;
                  } else {
                    console.log('nope');
                    let key = convRef.push(conversations).key;
                    convRef.child(key).update({uniqueId: key});
                    setConversationId(key);
                    userChatList(key);
                    return;
                  }
                }
              });
            } else {
              let key = convRef.push(conversations).key;
              convRef.child(key).update({uniqueId: key});
              setConversationId(key);
              userChatList(key);
            }
          });
        });
      } else {
        getCurrentUser().then((value) => {
          const {uniqueId, members} = item;
          let name = members.filter((member) => member !== value.email);
          navigation.setOptions({
            title: name,
          });
          setEmail(value.email);
          setConversationId(uniqueId);
          userChatList(uniqueId);
        });
      }

      // chats(item.email);
    } else {
      // do componentDidUpdate logic
    }
  }, []);
  const userChatList = (conversationId) => {
    database()
      .ref()
      .child(Config.FIREBASE_CHATS)
      .child(conversationId)
      .orderByChild('datetime')
      .on('value', function (snapshot) {
        let messages = [];
        snapshot.forEach(function (childSnapshot) {
          messages.push(childSnapshot.val());
        });
        setMessages(messages.reverse());
      });
  };

  return (
    <View style={[Layout.fill]}>
      <FlatList
        style={[Gutters.regularHPadding, {flexGrow: 1}]}
        data={messages}
        renderItem={({item}) => {
          return (
            <Message text={item.message} isSender={item.sendBy === email} />
          );
        }}
        keyExtractor={(item, index) => index.toString()}
        inverted
        // onScrollBeginDrag={this.onScrollBeginDrag}
        // onScrollEndDrag={this.onScrollEndDrag}
      />
      <MessageBar
        conversationId={conversationId}
        onLastMessage={(lastMessage, lastMessageDate) => {
          setLastMessage(lastMessage);
          setLastMessageDate(lastMessageDate);
        }}
      />
    </View>
  );
};

export default IndexChatContainer;
