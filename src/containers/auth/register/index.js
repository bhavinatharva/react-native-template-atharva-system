import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, TouchableOpacity, Text, TextInput} from 'react-native';
import {Brand, Button, Loader} from '@components';
import {useTheme} from '@theme';
import {register} from '@store/auth/Authentication';
import {useTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CheckConnection from '@utils/CheckConnection';
import {showMessage, hideMessage} from 'react-native-flash-message';

const IndexRegisterContainer = () => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const mounted = useRef();

  const registerResponse = useSelector((state) => state.auth.register.data);
  const registerLoading = useSelector((state) => state.auth.register.loading);
  const registerError = useSelector((state) => state.auth.register.error);

  const isConnected = CheckConnection();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
    } else {
      // do componentDidUpdate logic

      if (registerError) {
        showMessage({
          message: registerError.message,
          type: 'danger',
        });
      }
    }
  }, [registerResponse, registerError]);

  const doRegister = () => {
    if (email.trim() === '') {
      showMessage({
        message: 'Enter Email',
        type: 'danger',
      });
      return;
    }
    if (password.trim() === '') {
      showMessage({
        message: 'Enter Password',
        type: 'danger',
      });
      return;
    }
    if (!isConnected) {
      showMessage({
        message: 'No Internet',
        type: 'danger',
      });
      return;
    }
    dispatch(register.action({email: email, password: password}));
  };

  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <KeyboardAwareScrollView
        style={[Layout.fill]}
        keyboardShouldPersistTaps="always">
        <View style={[Layout.fill]}>
          <View style={[Layout.colCenter]}>
            <Brand source={Images.logo} />
          </View>
          <TextInput
            placeholder={t('email')}
            onChangeText={(text) => setEmail(text)}
            keyboardType="email-address"
            value={email}
            style={[Common.textInput, Fonts.textRegular]}
          />
          <TextInput
            placeholder={t('password')}
            onChangeText={(text) => setPassword(text)}
            keyboardType="default"
            value={password}
            secureTextEntry
            style={[Common.textInput, Fonts.textRegular]}
          />
        </View>
      </KeyboardAwareScrollView>
      {registerLoading && <Loader />}
      <TouchableOpacity onPress={() => doRegister()}>
        <Button title={t('button.register')} />
      </TouchableOpacity>
    </View>
  );
};

export default IndexRegisterContainer;
