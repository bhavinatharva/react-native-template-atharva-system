import React from 'react';
import {Text, Image, View} from 'react-native';

import {useTheme} from '@theme';

const FBButton = ({
  title = 'Sign in with Facebook',
  buttonViewStyle,
  logoStyle,
  textStyle,
}) => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  return (
    <View style={{...Common.facebookStyle, ...buttonViewStyle}}>
      <Image
        source={Images.facebook}
        style={{...Common.fbIconStyle, ...logoStyle}}
      />
      <Text style={{...Fonts.textLarge,...Common.fbTextStyle, ...textStyle}}>
        {title}
      </Text>
    </View>
  );
};

export default FBButton;
