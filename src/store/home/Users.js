import {
  buildAsyncState,
  buildAsyncReducers,
  buildAsyncActions,
} from '@thecodingmachine/redux-toolkit-wrapper';
import {doGetUsers, doGetUserDetail,doAddUser,doEditUser} from '@services/home/Users';

const users = {
  initialState: buildAsyncState('users'),
  action: buildAsyncActions('doGetUsers', doGetUsers),
  reducers: buildAsyncReducers({
    itemKey: 'users',
    errorKey: 'users.error', // Optionally, if you scoped variables, you can use a key with dot notation
    loadingKey: 'users.loading',
  }),
};
const userdetail = {
  initialState: buildAsyncState('userdetail'),
  action: buildAsyncActions('doGetUserDetail', doGetUserDetail),
  reducers: buildAsyncReducers({
    itemKey: 'userdetail',
    errorKey: 'userdetail.error', // Optionally, if you scoped variables, you can use a key with dot notation
    loadingKey: 'userdetail.loading',
  }),
};

const addUser = {
  initialState: buildAsyncState('addUser'),
  action: buildAsyncActions('doAddUser', doAddUser),
  reducers: buildAsyncReducers({
    itemKey: 'addUser',
    errorKey: 'addUser.error', // Optionally, if you scoped variables, you can use a key with dot notation
    loadingKey: 'addUser.loading',
  }),
};
const editUser = {
  initialState: buildAsyncState('editUser'),
  action: buildAsyncActions('doEditUser', doEditUser),
  reducers: buildAsyncReducers({
    itemKey: 'editUser',
    errorKey: 'editUser.error', // Optionally, if you scoped variables, you can use a key with dot notation
    loadingKey: 'editUser.loading',
  }),
};

export {users, userdetail,addUser,editUser};
