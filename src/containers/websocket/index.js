import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, TouchableOpacity, Text, TextInput, Button} from 'react-native';
import {Brand, Loader} from '@components';
import {useTheme} from '@theme';
import {addUser} from '@store/home/Users';
import {useTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CheckConnection from '@utils/CheckConnection';
import {isValidAmount} from '@utils/validate';
import {showMessage, hideMessage} from 'react-native-flash-message';
import RazorpayCheckout from 'react-native-razorpay';
import {Config} from '@config';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const IndexWebSocketContainer = ({navigation}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const mounted = useRef();
  let socket = new WebSocket('wss://echo.websocket.org/');
  const addUserResponse = useSelector((state) => state.home.addUser.data);
  const addUserLoading = useSelector((state) => state.home.addUser.loading);
  const addUserError = useSelector((state) => state.home.addUser.error);
  const doRedirect = (screen) => {
    navigation.navigate(screen);
  };
  const [open, setOpen] = useState(false);

  const isConnected = CheckConnection();
  const emit = () => {
    setOpen(!open);
    socket.send('It worked!');
  };
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
      socket.onopen = () =>
        socket.send(
          JSON.stringify({type: 'greet', payload: 'Hello Mr. Server!'}),
        );
      socket.onmessage = ({data}) => console.log(JSON.parse(data).payload);
    } else {
      // do componentDidUpdate logic
      if (addUserResponse) {
        showMessage({
          message: 'Add User Success',
          type: 'success',
        });
      }
      if (addUserError) {
        showMessage({
          message: addUserError.message,
          type: 'danger',
        });
      }
    }
  }, [addUserResponse, addUserError]);

  const LED = {
    backgroundColor: open ? 'lightgreen' : 'red',
    height: 30,
    position: 'absolute',
    flexDirection: 'row',
    bottom: 0,
    width: 100,
    height: 100,
    top: 120,
    borderRadius: 40,
    justifyContent: 'space-between',
  };

  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <Button
        onPress={() => emit()}
        title={open ? 'Turn off' : 'Turn on'}
        color="#21ba45"
        accessibilityLabel="Learn more about this purple button"
      />
      <View style={LED}></View>
    </View>
  );
};

export default IndexWebSocketContainer;
