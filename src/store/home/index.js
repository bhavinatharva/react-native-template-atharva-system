import {buildSlice} from '@thecodingmachine/redux-toolkit-wrapper';
import {users, userdetail, addUser,editUser} from './Users';
import {checkLogin,getCurrentUser} from './App';

// This state is common to all the "user" module, and can be modified by any "user" reducers
const sliceInitialState = {
  item: undefined,
};

export default buildSlice(
  'home',
  [users, checkLogin, userdetail, addUser,editUser,getCurrentUser],
  sliceInitialState,
).reducer;
