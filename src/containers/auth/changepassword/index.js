import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, TextInput,TouchableOpacity} from 'react-native';
import {useTheme} from '@theme';
import {login} from '@store/auth/Authentication';
import {setLogin} from '@services/home/App';
import {Button, Loader} from '@components';
import {useTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {showMessage, hideMessage} from 'react-native-flash-message';
import CheckConnection from '@utils/CheckConnection';
import {navigateAndReset} from '@navigators/root';

const IndexChangePasswordContainer = ({navigation}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const mounted = useRef();
  const loginResponse = useSelector((state) => state.auth.login.data);
  const loginLoading = useSelector((state) => state.auth.login.loading);
  const loginError = useSelector((state) => state.auth.login.error);

  const isConnected = CheckConnection();
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
    } else {
      // do componentDidUpdate logic

      if (loginError) {
        showMessage({
          message: loginError.message,
          type: 'danger',
        });
      }
      if (loginResponse) {
        if (loginResponse.token) {
          setLogin(true);
          navigateAndReset('Home');
        }
      }
    }
  }, [loginResponse, loginError]);

  const doSubmit = () => {
    if (newPassword.trim() === '') {
      showMessage({
        message: 'Enter New Password',
        type: 'danger',
      });
      return;
    }
    if (confirmPassword.trim() === '') {
      showMessage({
        message: 'Enter Confirm Password',
        type: 'danger',
      });
      return;
    }
    if (confirmPassword.trim() !== newPassword.trim()) {
      showMessage({
        message: 'Password should be match',
        type: 'danger',
      });
      return;
    }
    if (!isConnected) {
      showMessage({
        message: 'No Internet',
        type: 'danger',
      });
      return;
    }
    dispatch(login.action({password: newPassword, password: confirmPassword}));
  };

  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <KeyboardAwareScrollView
        style={[Layout.fill]}
        keyboardShouldPersistTaps="always">
        <View style={[Layout.fill]}>
          <TextInput
            placeholder={t('new_password')}
            onChangeText={(text) => setNewPassword(text)}
            keyboardType="default"
            value={newPassword}
            secureTextEntry
            style={[Common.textInput, Fonts.textRegular]}
          />
          <TextInput
            placeholder={t('confirm_password')}
            onChangeText={(text) => setConfirmPassword(text)}
            keyboardType="default"
            value={confirmPassword}
            secureTextEntry
            style={[Common.textInput, Fonts.textRegular]}
          />
          <TouchableOpacity onPress={() => doSubmit()}>
            <Button title={t('button.submit')} />
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
      {loginLoading && <Loader />}
    </View>
  );
};

export default IndexChangePasswordContainer;
