import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, Text, FlatList, TouchableOpacity} from 'react-native';
import {ConversationListItem, Loader, ListButton} from '@components';
import {useTheme} from '@theme';
// import {users as GetUsers} from '@store/home/Users';
import {useTranslation} from 'react-i18next';
import {showMessage, hideMessage} from 'react-native-flash-message';
import CheckConnection from '../../utils/CheckConnection';
import database from '@react-native-firebase/database';
import {Config} from '@config';
import {getCurrentUser} from '@services/home/App';

const IndexConversationsContainer = ({navigation}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const isConnected = CheckConnection();
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [conversations, setConversations] = useState([]);
  const [page, setPage] = useState(1);
  const [per_page, setPerPage] = useState(10);
  const [lastPage, setLastPage] = useState(1);
  const convRef = database().ref().child(`${Config.FIREBASE_CONVERSATIONS}`);

  // const usersResponse = useSelector((state) => state.home.users.data);
  // const usersLoading = useSelector((state) => state.home.users.loading);
  // const usersError = useSelector((state) => state.home.users.error);
  const mounted = useRef();
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
      setLoading(true);
      getCurrentUser().then((value) => {
        console.log(`value.email`, value.email);
        convRef.orderByChild('members').on('value', (snapshot) => {
          if (snapshot.exists()) {
            let conversations = [];
            snapshot.forEach((data) => {
              if (data.val().members.includes(value.email)) {
                console.log(`data.val()dsdsd`);

                let name = data
                  .val()
                  .members.filter((member) => member !== value.email);
                var conversation = Object.assign({}, data.val());
                conversation.name = name;
                conversations.push(conversation);
              }
            });
            setLoading(false);
            setConversations(conversations);
          }
        });
      });
      // dispatch(GetUsers.action({page: page, per_page: per_page}));
    } else {
      setRefreshing(false);
      setLoading(false);
    }
  }, []); //usersResponse, usersError

  const handleRefresh = () => {
    setPage(1);
    setRefreshing(true);
    // dispatch(GetUsers.action({page: page, per_page: per_page}));
  };
  const handleLoadMore = () => {
    if (page !== lastPage) {
      setPage(page + 1);
      // dispatch(GetUsers.action({page: page + 1, per_page: per_page}));
    }
  };
  const doEditDetail = (item) => {
    navigation.navigate('EditUser', {item: item, isEdit: true});
  };
  const doOpenDetail = (item) => {
    navigation.navigate('Chat', {item: item, from: 'Conversations'});
  };
  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <Text style={[Common.text, Fonts.textLarge]}>{t('conversations')}</Text>
      <View style={[Layout.fill]}>
        <FlatList
          data={conversations}
          onRefresh={handleRefresh}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.01}
          refreshing={refreshing}
          renderItem={({item}) => (
            <TouchableOpacity onPress={() => doOpenDetail(item)}>
              <ConversationListItem
                name={item.name}
                lastMessage={item.lastMessage}
                lastMessageSent={item.lastMessageSent}
              />
            </TouchableOpacity>
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>

      {loading ? <Loader /> : null}
    </View>
  );
};

export default IndexConversationsContainer;
