import React, {useState} from 'react';
import {
  TextInput,
  TouchableOpacity,
  Image,
  View,
  ActivityIndicator,
} from 'react-native';
import {useTranslation} from 'react-i18next';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useTheme} from '@theme';
import database from '@react-native-firebase/database';
import {Config} from '@config';
import moment from 'moment';
import {getCurrentUser} from '@services/home/App';

const MessageBar = ({conversationId, onLastMessage}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images, Colors} = useTheme();
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');
  const onSend = async (message, conversationId) => {
    if (message.trim() === '') {
      return;
    }
    setLoading(true);
    const chatRef = database()
      .ref()
      .child(Config.FIREBASE_CHATS)
      .child(conversationId);
    let lastMessage = message;
    let lastMessageDate = moment(new Date()).format('YYYY/MM/DDH HH:mm:ss');
    getCurrentUser().then((value) => {
      let chat = {
        message: lastMessage,
        conversationId: conversationId,
        datetime: lastMessageDate,
        sendBy: value.email,
      };
      chatRef.push(chat, () => {
        setLoading(false);
        onLastMessage({lastMessage, lastMessageDate});
        setMessage('');
        database()
          .ref(`${Config.FIREBASE_CONVERSATIONS}/${conversationId}`)
          .update({lastMessage: lastMessage, lastMessageSent: lastMessageDate});
      });
    });

    console.log(`message`, message);
  };
  return (
    <View style={[Layout.rowHCenter, Common.backgroundWhite, {padding: 15}]}>
      <TextInput
        placeholder={t('write_message')}
        onChangeText={(text) => setMessage(text)}
        keyboardType="default"
        value={message}
        style={[
          Common.textInput,
          Fonts.textRegular,
          Layout.fill,
          {borderRadius: 20, padding: 5, paddingStart: 15},
        ]}
      />
      {loading ? (
        <View style={{marginStart: 5, marginEnd: 5}}>
          <ActivityIndicator color={Colors.primary} size="small" />
        </View>
      ) : null}
      <TouchableOpacity
        style={{
          width: wp(10),
          height: wp(10),
          borderRadius: wp(5),
          backgroundColor: Colors.primaryDark,
          justifyContent: 'center',
          alignItems: 'center',
          marginStart: 10,
        }}
        onPress={() => onSend(message, conversationId)}>
        <Image
          resizeMode="contain"
          source={Images.send}
          style={{width: wp(5), height: wp(5)}}
        />
      </TouchableOpacity>
    </View>
  );
};

export default MessageBar;
