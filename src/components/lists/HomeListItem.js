import React from 'react';
import {View, Image, Text, TouchableOpacity, Touchable} from 'react-native';
import {useTheme} from '@theme';
import {ProgressiveImage} from '@components';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const HomeListItem = ({email, first_name, last_name, avatar, doOpenDetail}) => {
  const {Common, Fonts, Gutters, Layout, Images, Colors} = useTheme();

  return (
    <View style={[Common.gridItem]}>
      <TouchableOpacity
        style={{flex: 1, height: hp(25)}}
        onPress={() => doOpenDetail()}>
        <ProgressiveImage
          style={{flex: 1, height: hp(25)}}
          thumbnailSource={Images.placeholder}
          source={{
            uri: avatar,
          }}
        />
      </TouchableOpacity>
      <View
        style={{
          position: 'absolute',
          bottom: 0,
          width: '100%',
          start: 0,
          end: 0,
          backgroundColor: 'rgba(0,0,0,0.8)',
        }}>
        <Text
          style={[
            Fonts.textRegular,
            Common.listItemText,
            {color: Colors.white},
          ]}>
          Name : {first_name} {last_name}
        </Text>
      </View>
      <View
        style={{
          position: 'absolute',
          end: 0,
          top: 0,
          marginEnd: 15,
          marginTop: 15,
        }}>
        <TouchableOpacity>
          <Image
            style={{
              width: wp(5),
              height: wp(5),
              tintColor: Colors.white,
              shadowRadius: 6,
              shadowOpacity: 1,
              shadowOffset: {width: 3, height: 6},
            }}
            source={Images.favorite}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default HomeListItem;
