import React, {useEffect, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  IndexSplashContainer,
  IndexLoginContainer,
  IndexRegisterContainer,
  IndexForgotPasswordContainer,
  IndexUserDetailContainer,
  IndexAddUserContainer,
  IndexEditUserContainer,
  IndexStartUpContainer,
  IndexChangePasswordContainer,
  IndexChatContainer,
  IndexPaymentContainer,
  IndexAddCardContainer,
  IndexWebSocketContainer,
  IndexDatingContainer
} from '@containers';
import {NavigationContainer} from '@react-navigation/native';
import {navigationRef} from '@navigators/root';
import {TabIcon} from '@components';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {SafeAreaView, StatusBar, TouchableOpacity} from 'react-native';
import {useTheme} from '@theme';
import {AppearanceProvider} from 'react-native-appearance';
import {MainNavigator} from './index';
import {DrawerActions} from '@react-navigation/native';

const Stack = createStackNavigator();

// @refresh reset
const ApplicationNavigator = () => {
  const {Layout, darkMode, NavigationTheme, Images} = useTheme();
  const {colors} = NavigationTheme;

  return (
    <AppearanceProvider>
      <SafeAreaView style={[Layout.fill, {backgroundColor: colors.card}]}>
        <NavigationContainer theme={NavigationTheme} ref={navigationRef}>
          <StatusBar barStyle={darkMode ? 'light-content' : 'dark-content'} />
          <Stack.Navigator>
            <Stack.Screen
              name="Splash"
              component={IndexSplashContainer}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="StartUp"
              component={IndexStartUpContainer}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Login"
              component={IndexLoginContainer}
              options={{headerShown: false}}
            />
            <Stack.Screen name="Register" component={IndexRegisterContainer} />
            <Stack.Screen
              name="ForgotPassword"
              component={IndexForgotPasswordContainer}
            />
            <Stack.Screen
              name="Home"
              component={MainNavigator}
              options={({navigation, route}) => ({
                headerLeft: (props) => (
                  <TouchableOpacity
                    style={{margin: 15}}
                    onPress={() =>
                      navigation.dispatch(DrawerActions.toggleDrawer())
                    }>
                    <TabIcon
                      source={Images.menu}
                      height={wp(6)}
                      width={wp(6)}
                    />
                  </TouchableOpacity>
                ),
              })}
            />
            <Stack.Screen
              name="UserDetail"
              component={IndexUserDetailContainer}
              options={{
                title: '--',
              }}
            />
            <Stack.Screen
              name="AddUser"
              component={IndexAddUserContainer}
              options={{
                title: 'Add User',
              }}
            />
            <Stack.Screen
              name="EditUser"
              component={IndexEditUserContainer}
              options={{
                title: 'Edit User',
              }}
            />
            <Stack.Screen
              name="ChangePassword"
              component={IndexChangePasswordContainer}
              options={{
                title: 'Change Password',
              }}
            />
            <Stack.Screen
              name="Chat"
              component={IndexChatContainer}
              options={{
                title: '--',
              }}
            />
            <Stack.Screen
              name="Payment"
              component={IndexPaymentContainer}
              options={{
                title: 'Payment',
              }}
            />
            <Stack.Screen
              name="AddCard"
              component={IndexAddCardContainer}
              options={{
                title: 'Add Card',
              }}
            />
            <Stack.Screen
              name="WebSocket"
              component={IndexWebSocketContainer}
              options={{
                title: 'Web Socket',
              }}
            />
            <Stack.Screen
              name="Dating"
              component={IndexDatingContainer}
              options={{
                title: 'Dating',
              }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </SafeAreaView>
    </AppearanceProvider>
  );
};

export default ApplicationNavigator;
