import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {UserListItem, Loader} from '@components';
import {useTheme} from '@theme';
import {userdetail as GetUserDetail} from '@store/home/Users';
import {useTranslation} from 'react-i18next';
import {showMessage, hideMessage} from 'react-native-flash-message';
import CheckConnection from '../../utils/CheckConnection';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Colors} from 'react-native/Libraries/NewAppScreen';
const IndexUserDetailContainer = ({route, navigation}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const isConnected = CheckConnection();
  const [email, setEmail] = useState('--');
  const [first_name, setFirstName] = useState('--');
  const [last_name, setLastName] = useState('');
  const [avatar, setAvatar] = useState('');
  const {item} = route.params;

  const userDetailResponse = useSelector((state) => state.home.userdetail.data);
  const userDetailLoading = useSelector(
    (state) => state.home.userdetail.loading,
  );
  const userDetailError = useSelector((state) => state.home.userdetail.error);
  const mounted = useRef();

  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
      dispatch(GetUserDetail.action({id: item.id}));
    } else {
      // do componentDidUpdate logic
      if (userDetailError) {
        showMessage({
          message: userDetailError.message,
          type: 'danger',
        });
      }
      if (userDetailResponse) {
        navigation.setOptions({
          title: `${userDetailResponse.data.first_name} ${userDetailResponse.data.last_name}`,
        });
        setEmail(userDetailResponse.data.email);
        setFirstName(userDetailResponse.data.first_name);
        setLastName(userDetailResponse.data.last_name);
        setAvatar(userDetailResponse.data.avatar);
      }
    }
  }, [userDetailResponse, userDetailError]);

  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <Text style={[Common.text, Fonts.textLarge]}>{t('user_detail')}</Text>
      <View style={[Layout.fill]}>
        <UserListItem
          email={email}
          first_name={first_name}
          last_name={last_name}
          avatar={avatar}
        />
      </View>
      <View style={{position: 'absolute', end: wp(6), bottom: hp(6)}}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('Chat', {item: item, from: 'UserDetail'})
          }
          style={{
            width: wp(12),
            height: wp(12),
            borderRadius: wp(6),
            backgroundColor: Colors.primary,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            resizeMode="contain"
            source={Images.chat}
            style={{width: wp(6), height: wp(6)}}
          />
        </TouchableOpacity>
      </View>
      {userDetailLoading && <Loader />}
    </View>
  );
};

export default IndexUserDetailContainer;
