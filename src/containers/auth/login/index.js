import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, TouchableOpacity, Text, TextInput} from 'react-native';
import {
  Brand,
  Button,
  Loader,
  FBButton,
  GoogleButton,
  AppleButton,
} from '@components';
import {useTheme} from '@theme';
import {login} from '@store/auth/Authentication';
import {setLogin, setCurrentUser} from '@services/home/App';

import {useTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {showMessage, hideMessage} from 'react-native-flash-message';
import CheckConnection from '@utils/CheckConnection';
import {navigateAndReset} from '@navigators/root';
import {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
  AccessToken,
} from 'react-native-fbsdk';
import {appleAuth} from '@invertase/react-native-apple-authentication';

import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import database from '@react-native-firebase/database';
import {Config} from '@config';

const IndexLoginContainer = ({navigation}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const mounted = useRef();
  const loginResponse = useSelector((state) => state.auth.login.data);
  const loginLoading = useSelector((state) => state.auth.login.loading);
  const loginError = useSelector((state) => state.auth.login.error);
  const responseFbLogin = (error, result) => {
    if (error) {
      console.log(error);
    } else {
      console.log(result);
      setLogin(true);
      navigateAndReset('Home');
    }
  };
  const isConnected = CheckConnection();
  const [credentialStateForUser, updateCredentialStateForUser] = useState(-1);
  const [email, setEmail] = useState('eve.holt@reqres.in');
  const [password, setPassword] = useState('cityslicka');
  const [userData, setUserData] = useState(undefined);
  const userRef = database().ref().child(Config.FIREBASE_USERS);
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
      GoogleSignin.configure({
        scopes: [
          'https://www.googleapis.com/auth/userinfo.profile',
          'https://www.googleapis.com/auth/userinfo.email',
        ],
        webClientId:
          '618912750014-ba1b5em08431vq7ta7u2ijcv809kgfoj.apps.googleusercontent.com',
      });
    } else {
      // do componentDidUpdate logic

      if (loginError) {
        showMessage({
          message: loginError.message,
          type: 'danger',
        });
      }
      if (loginResponse) {
        if (loginResponse.token) {
          setCurrentUser({email: email, token: loginResponse.token});
          let user = {
            email: email,
            token: loginResponse.token,
          };
          userRef
            .orderByChild('email')
            .equalTo(email)
            .once('value', (snapshot) => {
              if (snapshot.exists()) {
                var key = Object.keys(snapshot.val())[0];
                console.log(`key`, key);
                userRef.child(key).set(user);
              } else {
                userRef.push(user);
              }
              setLogin(true);
              navigateAndReset('Home');
            });
        }
      }
    }
  }, [loginResponse, loginError]);
  const fetchAndUpdateCredentialState = async (
    updateCredentialStateForUser,
  ) => {
    if (user === null) {
      updateCredentialStateForUser('N/A');
    } else {
      const credentialState = await appleAuth.getCredentialStateForUser(user);
      if (credentialState === appleAuth.State.AUTHORIZED) {
        updateCredentialStateForUser('AUTHORIZED');
      } else {
        updateCredentialStateForUser(credentialState);
      }
    }
  };
  const onAppleButtonPress = async (updateCredentialStateForUser) => {
    if (!appleAuth.isSupported) {
      showMessage({
        message: 'Apple Authentication is not supported on this device.',
        type: 'danger',
      });
      return;
    }
    // start a login request
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });

      console.log('appleAuthRequestResponse', appleAuthRequestResponse);

      const {
        user: newUser,
        email,
        nonce,
        identityToken,
        realUserStatus /* etc */,
      } = appleAuthRequestResponse;

      user = newUser;

      fetchAndUpdateCredentialState(
        updateCredentialStateForUser,
      ).catch((error) => updateCredentialStateForUser(`Error: ${error.code}`));

      if (identityToken) {
        // e.g. sign in with Firebase Auth using `nonce` & `identityToken`
        console.log(nonce, identityToken);
      } else {
        // no token - failed sign-in?
      }

      if (realUserStatus === appleAuth.UserStatus.LIKELY_REAL) {
        console.log("I'm a real person!");
      }

      console.warn(`Apple Authentication Completed, ${user}, ${email}`);
    } catch (error) {
      if (error.code === appleAuth.Error.CANCELED) {
        console.warn('User canceled Apple Sign in.');
      } else {
        console.error(error);
      }
    }
  };
  const doFacebookLogin = () => {
    if (!isConnected) {
      showMessage({
        message: 'No Internet',
        type: 'danger',
      });
      return;
    }
    LoginManager.logInWithPermissions(['public_profile']).then(
      function (result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          console.log('Login success with permissions: ', result);

          AccessToken.getCurrentAccessToken().then((data) => {
            let accessToken = data.accessToken;
            console.log('accessToken', accessToken);
            const infoRequest = new GraphRequest(
              '/me',
              {
                accessToken: accessToken,
                parameters: {
                  fields: {
                    string: 'email,name,first_name,middle_name,last_name',
                  },
                },
              },
              responseFbLogin,
            );
            // Start the graph request.
            new GraphRequestManager().addRequest(infoRequest).start();
          });
        }
      },
      function (error) {
        console.log('Login fail with error: ' + error);
      },
    );
  };
  const doGoogleLogin = async () => {
    try {
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log('userInfo', userInfo);
      setUserData(userInfo);
      setLogin(true);
      navigateAndReset('Home');
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
      showMessage({
        message: error.message,
        type: 'danger',
      });
    }
  };
  const doRedirect = (screen) => {
    navigation.navigate(screen);
  };
  const doLogin = () => {
    if (email.trim() === '') {
      showMessage({
        message: 'Enter Email',
        type: 'danger',
      });
      return;
    }
    if (password.trim() === '') {
      showMessage({
        message: 'Enter Password',
        type: 'danger',
      });
      return;
    }
    if (!isConnected) {
      showMessage({
        message: 'No Internet',
        type: 'danger',
      });
      return;
    }
    dispatch(login.action({email: email, password: password}));
  };

  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <KeyboardAwareScrollView
        style={[Layout.fill]}
        keyboardShouldPersistTaps="always">
        <View style={[Layout.fill]}>
          <View style={[Layout.colCenter]}>
            <Brand source={Images.logo} />
          </View>
          <TextInput
            placeholder={t('email')}
            onChangeText={(text) => setEmail(text)}
            keyboardType="email-address"
            value={email}
            style={[Common.textInput, Fonts.textRegular]}
          />
          <TextInput
            placeholder={t('password')}
            onChangeText={(text) => setPassword(text)}
            keyboardType="default"
            value={password}
            secureTextEntry
            style={[Common.textInput, Fonts.textRegular]}
          />
          <TouchableOpacity onPress={() => doLogin()}>
            <Button title={t('button.login')} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => doFacebookLogin()}>
            <FBButton />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => doGoogleLogin()}>
            <GoogleButton />
          </TouchableOpacity>
          {appleAuth.isSupported ? (
            <TouchableOpacity
              onPress={() => onAppleButtonPress(updateCredentialStateForUser)}>
              <AppleButton />
            </TouchableOpacity>
          ) : null}

          <View style={[Layout.row]}>
            <TouchableOpacity
              style={[Layout.fill]}
              onPress={() => doRedirect('Register')}>
              <Text style={[Common.text, Fonts.textLarge]}>
                {t('new_account')}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[Layout.fill]}
              onPress={() => doRedirect('ForgotPassword')}>
              <Text style={[Common.text, Fonts.textLarge]}>
                {t('forgot_password')}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
      {loginLoading && <Loader />}
    </View>
  );
};

export default IndexLoginContainer;
