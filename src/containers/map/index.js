import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, Text, Platform, TouchableOpacity} from 'react-native';
import {ConversationListItem, Loader, ListButton} from '@components';
import {useTheme} from '@theme';
// import {users as GetUsers} from '@store/home/Users';
import {useTranslation} from 'react-i18next';
import {showMessage, hideMessage} from 'react-native-flash-message';
import CheckConnection from '../../utils/CheckConnection';
import {Config} from '@config';
import {getCurrentUser} from '@services/home/App';
import MapView, {
  Marker,
  AnimatedRegion,
  Polyline,
  PROVIDER_GOOGLE,
} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import haversine from 'haversine';

const IndexMapContainer = ({navigation}) => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();
  const dispatch = useDispatch();
  const LATITUDE_DELTA = 0.009;
  const LONGITUDE_DELTA = 0.009;
  const LATITUDE = 37.78825;
  const LONGITUDE = -122.4324;
  const isConnected = CheckConnection();
  const [loading, setLoading] = useState(false);
  const [latitude, setLatitude] = useState(LATITUDE);
  const [longitude, setLongitude] = useState(LONGITUDE);
  const [routeCoordinates, setRouteCoordinates] = useState([]);
  const [distanceTravelled, setDistance] = useState(0);
  const [prevLatLng, setPrevLatLng] = useState({});
  const [endCoordinate, setEndCoordinate] = useState({
    latitude: 22.7274,
    longitude: 71.6512,
  });
  const [startCoordinate, setStartCoordinate] = useState({
    latitude: 22.7289,
    longitude: 71.6535,
  });
  const [coordinate, setCoordinate] = useState(
    new AnimatedRegion({
      latitude: LATITUDE,
      longitude: LONGITUDE,
      latitudeDelta: 0,
      longitudeDelta: 0,
    }),
  );
  const calcDistance = (newLatLng) => {
    return haversine(prevLatLng, newLatLng) || 0;
  };
  useEffect(() => {
    const watchId = Geolocation.watchPosition(
      (position) => {
        const {latitude, longitude} = position.coords;
        const newCoordinate = {
          latitude,
          longitude,
        };
        if (Platform.OS === 'android') {
          if (markerRef && markerRef.current) {
            markerRef.current.animateMarkerToCoordinate(newCoordinate, 500);
          }
        } else {
          coordinate.timing(newCoordinate).start();
        }
        setLatitude(latitude);
        setLongitude(longitude);
        setRouteCoordinates((old) => [...old, newCoordinate]);
        setDistance(distanceTravelled + calcDistance(newCoordinate));
      },
      (error) => console.log(`error`, error),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
        distanceFilter: 10,
      },
    );
    return () => Geolocation.clearWatch(watchId);
  }, []);
  const getMapRegion = () => ({
    latitude: latitude,
    longitude: longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  });
  const mounted = useRef();
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
      setLoading(true);
      getCurrentUser().then((value) => {
        console.log(`value.email`, value.email);
        Geolocation.getCurrentPosition(
          (position) => {
            const {latitude, longitude} = position.coords;
            const newCoordinate = {
              latitude,
              longitude,
            };
            if (Platform.OS === 'android') {
              if (markerRef && markerRef.current) {
                markerRef.current.animateMarkerToCoordinate(newCoordinate, 500);
              }
            } else {
              coordinate.timing(newCoordinate).start();
            }
            setLatitude(latitude);
            setLongitude(longitude);
            setRouteCoordinates((old) => [...old, newCoordinate]);
            setDistance(distanceTravelled + calcDistance(newCoordinate));
          },
          (error) => console.log(`error`, error),
        );
      });
      // dispatch(GetUsers.action({page: page, per_page: per_page}));
    } else {
      setLoading(false);
    }
  }, []); //usersResponse, usersError
  const markerRef = useRef();
  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <MapView
        onMapReady={() => setLoading(false)}
        style={{flex: 1}}
        provider={PROVIDER_GOOGLE}
        region={getMapRegion()}>
        <Polyline
          coordinates={routeCoordinates}
          strokeWidth={5}
          strokeColor="#000"
          fillColor="rgba(255,0,0,0.5)"
        />
        <Marker coordinate={startCoordinate} title="A" />
        <Marker coordinate={endCoordinate} title="B" />
        <Marker.Animated
          ref={markerRef}
          coordinate={coordinate}
          image={Images.bike_marker}
        />
      </MapView>
      <View>
        <TouchableOpacity disabled>
          <Text style={[Common.text]}>
            {parseFloat(distanceTravelled).toFixed(2)} km
          </Text>
        </TouchableOpacity>
      </View>
      {loading ? <Loader /> : null}
    </View>
  );
};

export default IndexMapContainer;
