import React, {useEffect, useState, useRef} from 'react';
import {ActivityIndicator, View, Text, TouchableOpacity} from 'react-native';
import {useTheme} from '@theme';
import {useDispatch} from 'react-redux';
import {useTranslation} from 'react-i18next';
import {Brand} from '@components';
import Swiper from 'react-native-swiper';
import {navigateAndSimpleReset, navigateAndReset} from '@navigators/root';
import {setIntro} from '@services/home/App';

const IndexStartUpContainer = () => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();

  const {t} = useTranslation();
  const [currentIndex, setCurrentIndex] = useState(0);
  const [pages, setPages] = useState([
    {
      id: 0,
      title: 'Hello',
      description: 'Welcome to Start Up Page',
    },
    {
      id: 1,
      title: 'Thank you',
      description: 'Welcome to Start Up Page',
    },
    {
      id: 2,
      title: 'Thank you,Glad to work with you',
      description: 'Welcome to Start Up Page',
    },
  ]);
  const doClickNext = () => {
    console.log(currentIndex);
    if (inputEl && currentIndex < 2) {
      inputEl.scrollBy(1);
      return;
    }
    setIntro(true);
    navigateAndSimpleReset('Login');
  };
  const doClickPrevious = () => {
    console.log(currentIndex);
    if (inputEl && currentIndex > 0) {
      inputEl.scrollBy(-1);
    }
  };
  const renderSwipePage = (value) => {
    return (
      <View key={value.id} style={[Layout.fill, Layout.colCenter]}>
        <Text style={[Common.text]}>{value.title}</Text>
        <Text style={[Common.text]}>{value.description}</Text>
      </View>
    );
  };
  let inputEl = useRef(null);

  return (
    <View style={[Layout.fill]}>
      <Swiper
        ref={(ref) => (inputEl = ref)}
        onIndexChanged={setCurrentIndex}
        key={pages.length}
        showsButtons={false}
        loop={false}
        dot={<View style={[Common.dot]} />}
        activeDot={<View style={[Common.activeDot]} />}>
        {pages.map((value, index) => {
          return renderSwipePage(value);
        })}
      </Swiper>
      <View style={[Layout.rowHCenter]}>
        <TouchableOpacity
          disabled={currentIndex === 2}
          onPress={() => doClickPrevious()}
          style={[Layout.fill]}>
          <Text style={[Common.text]}>
            {currentIndex > 0 && currentIndex < 2 ? 'Previous' : ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => doClickNext()} style={[Layout.fill]}>
          <Text style={[Common.text, Fonts.textRight]}>
            {currentIndex < 2 ? 'Next' : 'Done'}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default IndexStartUpContainer;
