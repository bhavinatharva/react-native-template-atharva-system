import React, {useState, useEffect, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, TouchableOpacity, Text, TextInput} from 'react-native';
import {Brand, Button, Loader} from '@components';
import {useTheme} from '@theme';
import {addUser} from '@store/home/Users';
import {useTranslation} from 'react-i18next';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CheckConnection from '@utils/CheckConnection';
import {showMessage, hideMessage} from 'react-native-flash-message';
import cardValidator from 'card-validator';

const IndexAddCardContainer = () => {
  const {t} = useTranslation();
  const {Common, Fonts, Gutters, Layout, Colors, FontSize} = useTheme();
  const dispatch = useDispatch();
  const mounted = useRef();

  const addUserResponse = useSelector((state) => state.home.addUser.data);
  const addUserLoading = useSelector((state) => state.home.addUser.loading);
  const addUserError = useSelector((state) => state.home.addUser.error);

  const isConnected = CheckConnection();

  const [name, setName] = useState('');
  const [cardnumber, setCardNumber] = useState('');
  const [month, setMonth] = useState('');
  const [year, setYear] = useState('');
  const [cvv, setCVV] = useState('');
  const [months, setMonths] = useState([
    {
      value: '01',
    },
    {
      value: '02',
    },
    {
      value: '03',
    },
    {
      value: '03',
    },
    {
      value: '04',
    },
    {
      value: '05',
    },
    {
      value: '06',
    },
    {
      value: '07',
    },
    {
      value: '08',
    },
    {
      value: '09',
    },
    {
      value: '10',
    },
    {
      value: '11',
    },
    {
      value: '12',
    },
  ]);
  const [years, setYears] = useState([]);
  const generateArrayOfYears = () => {
    var max = new Date().getFullYear();
    var min = max - 15;
    var years = [];

    for (var i = min; i <= max; i--) {
      years.push({value: i});
    }
    return years;
  };
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
      if (!isConnected) {
        showMessage({
          message: 'No Internet',
          type: 'danger',
        });
        return;
      }
      setYears(generateArrayOfYears());
    } else {
      // do componentDidUpdate logic
      if (addUserResponse) {
        showMessage({
          message: 'Add User Success',
          type: 'success',
        });
      }
      if (addUserError) {
        showMessage({
          message: addUserError.message,
          type: 'danger',
        });
      }
    }
  }, [addUserResponse, addUserError]);

  const doAddUser = () => {
    if (name.trim() === '') {
      showMessage({
        message: 'Enter Name',
        type: 'danger',
      });
      return;
    }
    if (cardnumber.trim() === '') {
      showMessage({
        message: 'Enter Card Number',
        type: 'danger',
      });
      return;
    }
    var numberValidation = cardValidator.number(cardnumber.trim());
    console.log(`numberValidation`, numberValidation);
    if (!numberValidation.isValid) {
      showMessage({
        message: 'Enter Valid Card Number',
        type: 'danger',
      });
      return;
    }
    if (month.trim() === '') {
      showMessage({
        message: 'Enter Month',
        type: 'danger',
      });
      return;
    }
    var monthValidation = cardValidator.expirationMonth(month.trim());
    console.log(`monthValidation`, monthValidation);
    if (!monthValidation.isValid) {
      showMessage({
        message: 'Enter Valid Month',
        type: 'danger',
      });
      return;
    }
    if (year.trim() === '') {
      showMessage({
        message: 'Enter Year',
        type: 'danger',
      });
      return;
    }
    var yearValidation = cardValidator.expirationYear(year.trim());
    console.log(`yearValidation`, yearValidation);
    if (!yearValidation.isValid) {
      showMessage({
        message: 'Enter Valid Year',
        type: 'danger',
      });
      return;
    }

    if (!isConnected) {
      showMessage({
        message: 'No Internet',
        type: 'danger',
      });
      return;
    }
    // dispatch(addUser.action({name: name, cardnumber: cardnumber}));
  };

  return (
    <View style={[Layout.fill, Gutters.regularHPadding]}>
      <KeyboardAwareScrollView
        style={[Layout.fill]}
        keyboardShouldPersistTaps="always">
        <View style={[Layout.fill]}>
          <TextInput
            placeholder={t('name')}
            onChangeText={(text) => setName(text)}
            keyboardType="default"
            value={name}
            style={[Common.textInput, Fonts.textRegular]}
          />
          <TextInput
            placeholder={t('cardnumber')}
            onChangeText={(text) => setCardNumber(text)}
            keyboardType="default"
            value={cardnumber}
            style={[Common.textInput, Fonts.textRegular]}
          />
          <View style={[Layout.rowHCenter]}>
            <TextInput
              placeholder={t('month')}
              onChangeText={(text) => setMonth(text)}
              keyboardType="numeric"
              value={month}
              maxLength={2}
              style={[
                Common.textInput,
                Fonts.textRegular,
                Layout.fill,
                {marginEnd: 5},
              ]}
            />
            <TextInput
              placeholder={t('year')}
              onChangeText={(text) => setYear(text)}
              keyboardType="numeric"
              value={year}
              maxLength={4}
              style={[
                Common.textInput,
                Fonts.textRegular,
                Layout.fill,
                {marginStart: 5},
              ]}
            />
          </View>
          <TextInput
            placeholder={t('cvv')}
            onChangeText={(text) => setCVV(text)}
            keyboardType="numeric"
            value={cvv}
            maxLength={4}
            style={[Common.textInput, Fonts.textRegular]}
          />
        </View>
      </KeyboardAwareScrollView>
      {addUserLoading && <Loader />}
      <TouchableOpacity onPress={() => doAddUser()}>
        <Button title={t('button.add_card')} />
      </TouchableOpacity>
    </View>
  );
};

export default IndexAddCardContainer;
