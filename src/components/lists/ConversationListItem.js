import React from 'react';
import {View, Image, Text} from 'react-native';
import {useTheme} from '@theme';

const ConversationListItem = ({name, lastMessage, lastMessageSent}) => {
  const {Common, Fonts, Gutters, Layout, Images} = useTheme();

  return (
    <View style={[Common.listItem]}>
      <Text style={[Fonts.textRegular, Common.listItemText]}>
        Name : {name}
      </Text>
      <Text style={[Fonts.textRegular, Common.listItemText]}>
        Message : {lastMessage}
      </Text>
      <Text style={[Fonts.textRegular, Common.listItemText]}>
        Send On : {lastMessageSent}
      </Text>
    </View>
  );
};

export default ConversationListItem;
