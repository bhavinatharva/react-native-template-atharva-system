/**
 * This file defines the base application styles.
 *
 * Use it to define generic component styles (e.g. the default text styles, default button styles...).
 */
import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

/**
 *
 * @param Theme can be spread like {Colors, NavigationColors, Gutters, Layout, Common, ...args}
 * @return {*}
 */
export default function ({Colors}) {
  return StyleSheet.create({
    button: {
      backgroundColor: Colors.primary,
      margin: 15,
      padding: 15,
    },
    loader: {
      backgroundColor: Colors.dimBlack,
      position: 'absolute',
      top: 0,
      bottom: 0,
      start: 0,
      end: 0,
    },
    backgroundWhite: {
      backgroundColor: Colors.white,
    },
    backgroundPrimary: {
      backgroundColor: Colors.primary,
    },
    backgroundReset: {
      backgroundColor: Colors.transparent,
    },
    textInput: {
      borderWidth: 1,
      borderColor: Colors.text,
      backgroundColor: Colors.inputBackground,
      color: Colors.text,
      padding: 15,
      marginTop: 10,
      marginBottom: 10,
    },
    text: {
      color: Colors.text,
      padding: 15,
      marginTop: 10,
      marginBottom: 10,
    },
    listItem: {
      borderRadius: 6,
      marginStart: 10,
      marginEnd: 10,
      marginBottom: 10,
      backgroundColor: Colors.white,
      padding: 10,
    },
    avatar: {
      height: wp(12),
      width: wp(12),
      borderRadius: wp(6),
      marginEnd: 10,
    },
    hiddenItem: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
      padding: 10,
      marginStart: 10,
      marginEnd: 10,
      marginBottom: 10,
      flex: 1,
    },
    listButton: {
      margin: 15,
      alignItems: 'center',
    },
    listImage: {
      height: wp(8),
      width: wp(8),
    },
    listItemText: {
      color: Colors.text,
      marginBottom: 5,
    },
    facebookStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#485a96',
      margin: 15,
      padding: 15,
    },
    fbIconStyle: {
      padding: 10,
      marginLeft: 15,
      height: wp(6),
      width: wp(6),
      resizeMode: 'stretch',
      alignSelf: 'center',
    },
    fbTextStyle: {
      color: Colors.white,
      marginLeft: 20,
      marginRight: 20,
    },
    googleStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors.white,
      margin: 15,
      padding: 15,
      borderWidth: 1,
      borderColor: Colors.primary,
    },
    googleTextStyle: {
      color: Colors.text,
      marginLeft: 20,
      marginRight: 20,
    },
    gridItem: {
      borderRadius: 6,
      flex: 1,
      height: hp(25),
      backgroundColor: Colors.white,
      maxWidth: '45%',
      marginBottom: 15,
    },
    dot: {
      backgroundColor: 'rgba(0,0,0,.2)',
      width: 5,
      height: 5,
      borderRadius: 4,
      marginLeft: 3,
      marginRight: 3,
      marginTop: 3,
      marginBottom: 3,
    },
    activeDot: {
      backgroundColor: '#000',
      width: 8,
      height: 8,
      borderRadius: 4,
      marginLeft: 3,
      marginRight: 3,
      marginTop: 3,
      marginBottom: 3,
    },
    appleStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: Colors.black,
      margin: 15,
      padding: 15,
    },
    appleIconStyle: {
      padding: 10,
      marginLeft: 15,
      height: wp(6),
      width: wp(6),
      resizeMode: 'stretch',
      alignSelf: 'center',
      tintColor: Colors.white,
    },
    chatSender: {
      backgroundColor: Colors.primaryDark,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      borderBottomLeftRadius: 10,
      padding: 10,
      maxWidth: '80%',
      marginBottom: 10,
      justifyContent:"flex-end",
      alignItems:"flex-end"
    },
    chatReceiver: {
      backgroundColor: Colors.primary,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      borderBottomRightRadius: 10,
      padding: 10,
      maxWidth: '80%',
      marginBottom: 10,
    },
  });
}
