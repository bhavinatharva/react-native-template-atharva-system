import {buildSlice} from '@thecodingmachine/redux-toolkit-wrapper';
import {login, register, forgotpassword} from './Authentication';

// This state is common to all the "user" module, and can be modified by any "user" reducers
const sliceInitialState = {
  item: undefined,
};

export default buildSlice(
  'auth',
  [login, register, forgotpassword],
  sliceInitialState,
).reducer;
